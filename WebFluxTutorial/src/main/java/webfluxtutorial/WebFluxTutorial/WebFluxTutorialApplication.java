package webfluxtutorial.WebFluxTutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebFluxTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebFluxTutorialApplication.class, args);
	}
}
